//
//  Stock.m
//  IMKB
//
//  Created by Macbook Air on 27/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import "Stock.h"

@implementation Stock
@synthesize symbol, price, difference, volume, buying, selling, hourInSeconds, dayPeakPrice, dayLowestPrice, total;

@end
