//
//  imkbListObject.h
//  IMKB
//
//  Created by Macbook Air on 29/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface imkbListObject : NSObject
@property (nonatomic, retain) NSString *symbol;
@property (nonatomic, retain) NSString *name;
@property float gain;
@property float fund;
@end
