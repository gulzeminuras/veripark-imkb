//
//  StockPrice.h
//  IMKB
//
//  Created by Macbook Air on 28/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StockPrice : NSObject
@property float price;
@property (nonatomic, retain) NSString *dateString;
@end
