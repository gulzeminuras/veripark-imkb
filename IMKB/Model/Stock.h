//
//  Stock.h
//  IMKB
//
//  Created by Macbook Air on 27/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Stock : NSObject
@property (nonatomic, retain) NSString *symbol;
@property float price;
@property float difference;
@property int volume;
@property float buying;
@property float selling;
@property double hourInSeconds;
@property float dayPeakPrice;
@property float dayLowestPrice;
@property int total;

@end
