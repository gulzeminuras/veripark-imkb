//
//  Constants.h
//  IMKB
//
//  Created by Macbook Air on 27/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - ServiceURLs
#define BASEURL @"http://mobileexam.veripark.com/mobileforeks/service.asmx"


#pragma mark - Periods
#define DAY @"Day"
#define WEEK @"Week"
#define MONTH @"Month"


#pragma mark - Other
#define ENCRYPTTOKEN @"EncryptToken"
#define CELLHEIGHT 35
#define GENERIC_ERROR_MESSAGE @"Bir hata oluştu. Lütfen tekrar deneyin!"
#define TOKEN_ERROR_MESSAGE @"Token hatası!"
#define CONNECTION_ERROR_MESSAGE @"Bağlantı hatası!"