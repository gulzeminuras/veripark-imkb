//
//  DateFormat.h
//  IMKB
//
//  Created by Macbook Air on 27/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateFormat : NSObject

+(NSString *) dateToString:(NSDate *) date;
+(NSString *) dateStringToSimpleString:(NSString *) dateString withYear:(BOOL) year;
+(NSString *)getTimeStringFromSeconds:(double)seconds;

@end
