//
//  DateFormat.m
//  IMKB
//
//  Created by Macbook Air on 27/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import "DateFormat.h"

@implementation DateFormat

+(NSString *) dateToString:(NSDate *) date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd:MM:yyyy HH:mm"];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"tr_TR"]];
    
    return  [dateFormatter stringFromDate:date];
}

+(NSString *) dateStringToSimpleString:(NSString *) dateString withYear:(BOOL)year{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *dateFromString = [dateFormatter dateFromString: dateString];
    if (year) {
        [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    }else {
        [dateFormatter setDateFormat:@"dd/MM"];
    }
    
    return  [dateFormatter stringFromDate:dateFromString];
}

+(NSString *)getTimeStringFromSeconds:(double)seconds
{
    NSDateComponentsFormatter *dcFormatter = [[NSDateComponentsFormatter alloc] init];
    dcFormatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehaviorPad;
    dcFormatter.allowedUnits = NSCalendarUnitHour | NSCalendarUnitMinute;
    dcFormatter.unitsStyle = NSDateComponentsFormatterUnitsStylePositional;
    return [dcFormatter stringFromTimeInterval:seconds];
}
@end
