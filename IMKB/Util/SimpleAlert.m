//
//  SimpleAlert.m
//  IMKB
//
//  Created by Macbook Air on 28/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import "SimpleAlert.h"

@implementation SimpleAlert
+(void) showSimpleAlert:(NSString *) message withController:(UIViewController *) controller{
    if ([UIAlertController class])
    {
        // use UIAlertController
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@""
                                   message:message
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Tamam" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       
                                                       
                                                   }];
        
        [alert addAction:ok];
        [controller presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        // use UIAlertView
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"Tamam"
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    
    
}

@end
