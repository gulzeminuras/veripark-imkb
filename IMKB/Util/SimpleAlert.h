//
//  SimpleAlert.h
//  IMKB
//
//  Created by Macbook Air on 28/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface SimpleAlert : NSObject
+(void) showSimpleAlert:(NSString *) message withController:(UIViewController *) controller;
@end
