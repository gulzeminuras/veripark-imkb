//
//  EncryptWS.m
//  IMKB
//
//  Created by Macbook Air on 27/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import "EncryptWS.h"

@implementation EncryptWS
@synthesize delegate, XMLTag;

- (id)initWithDelegate:(id<EncryptServiceDelegate>) del
{
    self = [super init];
    if (self) {
        self.delegate = del;
    }
    return self;
}

-(void) callEncryptService{
    @try {
        NSURL *urlm = [NSURL URLWithString:BASEURL];
        
        ServiceUrl = [NSMutableURLRequest requestWithURL:urlm];
        [ServiceUrl setCachePolicy:NSURLRequestUseProtocolCachePolicy];
        
        NSMutableDictionary *headers = [NSMutableDictionary dictionaryWithCapacity:0];
        [headers setObject:@"text/xml" forKey:@"Content-Type"];
        [headers setObject:@"utf-8" forKey:@"charset"];
        [ServiceUrl setAllHTTPHeaderFields:headers];
        [ServiceUrl setHTTPMethod:@"POST"];
        
        NSString *tmp = [[NSString alloc] initWithFormat:
                         @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
                        " <soapenv:Header/>"
                        " <soapenv:Body>"
                        " <tem:Encrypt>"
                        " <!--Optional:-->"
                        " <tem:request>RequestIsValid%@</tem:request>"
                        " </tem:Encrypt>"
                        " </soapenv:Body>"
                        " </soapenv:Envelope>", [DateFormat dateToString:[NSDate date]]];
        
//        NSLog(@"%@" ,tmp);
        NSData* aData = [tmp dataUsingEncoding: NSUTF8StringEncoding];
        
        [ServiceUrl setHTTPBody:aData];
        
        ServiceConnection=[[NSURLConnection alloc] initWithRequest:(NSURLRequest *)ServiceUrl delegate:self];
        
        if (ServiceConnection)
        {
            ServiceReceivedData= [NSMutableData data] ;
        }
        
    }
    @catch (NSException * error)
    {
        
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    if (connection == ServiceConnection){
        [ServiceReceivedData setLength:0];
    }
    
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data{
    if (connection == ServiceConnection){
        [ServiceReceivedData appendData:data];
    }
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate onEncryptFailed:CONNECTION_ERROR_MESSAGE];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if(connection == ServiceConnection)
    {
        ServiceConnection = nil;
        
        xmlParser = [[NSXMLParser alloc] initWithData:ServiceReceivedData];
        [xmlParser setDelegate:self];
        [xmlParser setShouldProcessNamespaces:YES];
        [xmlParser setShouldReportNamespacePrefixes:YES];
        [xmlParser setShouldResolveExternalEntities:YES];
        [xmlParser parse];
        
        if (resultSet != nil) {
            [[NSUserDefaults standardUserDefaults] setObject:resultSet forKey:ENCRYPTTOKEN];
            [delegate onEncryptSucceeded:[NSString stringWithFormat:@"Encryption succeeded - %@", resultSet]];
        }else {
            [delegate onEncryptFailed:GENERIC_ERROR_MESSAGE];
        }
    }
}

#pragma mark - NSXMLParserDelegate Methods

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if (parser == xmlParser){
        if (!XMLTag){
            self.XMLTag = [[NSMutableString alloc]init];
        }
        XMLTag = (NSMutableString *) [XMLTag stringByAppendingString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName

{
    if ([elementName isEqualToString:@"EncryptResult"]) {
        if (!XMLTag) {
            resultSet = @"";
            return;
        }
        resultSet = XMLTag;
    }
    XMLTag = nil;
}

@end
