//
//  StockListWS.h
//  IMKB
//
//  Created by Macbook Air on 27/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "Stock.h"
#import "StockPrice.h"
#import "imkbListObject.h"

@protocol StockListServiceDelegate <NSObject>
-(void) getStockListSucceeded:(NSMutableArray *) stockArray imkb100List:(NSMutableArray *) imkb100 imkb50List:(NSMutableArray *) imkb50 imkb30List:(NSMutableArray *) imkb30;
-(void) getStockListFailed:(NSString *) error;
@end

@protocol StockDetailServiceDelegate <NSObject>
-(void) getStockDetailSucceeded:(NSMutableArray *) stockDetailArray;
-(void) getStockDetailFailed:(NSString *) error;
@end

@interface StockListWS : NSObject  <NSXMLParserDelegate>{
    
    NSMutableURLRequest *ServiceUrl;
    NSURLConnection *ServiceConnection;
    NSMutableData *ServiceReceivedData;
    NSXMLParser *xmlParser;
    NSMutableString *XMLTag;
    
    NSString *success;
    NSMutableArray *stockArray;
    NSMutableArray *imkb100;
    NSMutableArray *imkb50;
    NSMutableArray *imkb30;

    NSMutableArray *stockDetailArray;
    Stock *stockObject;
    StockPrice *stockDetailObject;
    imkbListObject *imkbObj;
    BOOL stockDetail;
}

@property(nonatomic,retain)id<StockListServiceDelegate> delegate;
@property(nonatomic,retain)id<StockDetailServiceDelegate> detailDelegate;
@property(nonatomic,retain)NSMutableString *XMLTag;

- (id)initWithDelegate:(id<StockListServiceDelegate>) del;
- (id)initWithDetailDelegate:(id<StockDetailServiceDelegate>) del;

-(void) getStockList:(NSString *) requestedSymbol withPeriod: (NSString *) period;


@end
