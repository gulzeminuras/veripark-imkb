//
//  StockListWS.m
//  IMKB
//
//  Created by Macbook Air on 27/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import "StockListWS.h"

@implementation StockListWS
@synthesize delegate, detailDelegate, XMLTag;

- (id)initWithDelegate:(id<StockListServiceDelegate>) del
{
    self = [super init];
    if (self) {
        self.delegate = del;
    }
    return self;
}

- (id)initWithDetailDelegate:(id<StockDetailServiceDelegate>) del
{
    self = [super init];
    if (self) {
        self.detailDelegate = del;
    }
    return self;
}

-(void) getStockList:(NSString *)requestedSymbol withPeriod:(NSString *)period{
    @try {
        if ([requestedSymbol isEqualToString:@""]) {
            stockDetail = false;
        }else {
            stockDetail = true;
        }
        NSURL *urlm = [NSURL URLWithString:BASEURL];
        
        ServiceUrl = [NSMutableURLRequest requestWithURL:urlm];
        [ServiceUrl setCachePolicy:NSURLRequestUseProtocolCachePolicy];
        
        NSMutableDictionary *headers = [NSMutableDictionary dictionaryWithCapacity:0];
        [headers setObject:@"text/xml" forKey:@"Content-Type"];
        [headers setObject:@"utf-8" forKey:@"charset"];
        [ServiceUrl setAllHTTPHeaderFields:headers];
        [ServiceUrl setHTTPMethod:@"POST"];
        
        NSString *tmp = [[NSString alloc] initWithFormat:
                        @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
                        " <soapenv:Header/>"
                        " <soapenv:Body>"
                        " <tem:GetForexStocksandIndexesInfo>"
                        " <!--Optional:-->"
                        " <tem:request>"
                        " <tem:IsIPAD>true</tem:IsIPAD>"
                        " <!--Optional:-->"
                        " <tem:DeviceID>test</tem:DeviceID>"
                        " <!--Optional:-->"
                        " <tem:DeviceType>ipad</tem:DeviceType>"
                        " <!--Optional:-->"
                        " <tem:RequestKey>%@</tem:RequestKey>"
                        " <!--Optional:-->"
                        " %@"
                        " <tem:Period>%@</tem:Period>"
                        " </tem:request>"
                        " </tem:GetForexStocksandIndexesInfo>"
                        " </soapenv:Body>"
                        " </soapenv:Envelope>", [[NSUserDefaults standardUserDefaults] stringForKey:ENCRYPTTOKEN], requestedSymbol, period];
        
//        NSLog(@"%@" ,tmp);
        NSData* aData = [tmp dataUsingEncoding: NSUTF8StringEncoding];
        
        [ServiceUrl setHTTPBody:aData];
        
        ServiceConnection=[[NSURLConnection alloc] initWithRequest:(NSURLRequest *)ServiceUrl delegate:self];
        
        if (ServiceConnection)
        {
            ServiceReceivedData= [NSMutableData data] ;
        }
        
    }
    @catch (NSException * error)
    {
        
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    if (connection == ServiceConnection){
        [ServiceReceivedData setLength:0];
    }
    
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data{
    if (connection == ServiceConnection){
        [ServiceReceivedData appendData:data];
    }
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if (stockDetail) {
        [detailDelegate getStockDetailFailed:CONNECTION_ERROR_MESSAGE];
    }else {
        [delegate getStockListFailed:CONNECTION_ERROR_MESSAGE];
    }
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if(connection == ServiceConnection)
    {
        ServiceConnection = nil;
        
        NSLog(@"%@", [[NSString alloc] initWithData:ServiceReceivedData encoding:NSUTF8StringEncoding]);
        
        xmlParser = [[NSXMLParser alloc] initWithData:ServiceReceivedData];
        [xmlParser setDelegate:self];
        [xmlParser setShouldProcessNamespaces:YES];
        [xmlParser setShouldReportNamespacePrefixes:YES];
        [xmlParser setShouldResolveExternalEntities:YES];
        [xmlParser parse];
        
        if ([success isEqualToString:@"true"]) {
            if (stockDetail) {
                [detailDelegate getStockDetailSucceeded:stockDetailArray];
            }else {
                [delegate getStockListSucceeded:stockArray imkb100List:imkb100 imkb50List:imkb50 imkb30List:imkb30];
            }
        }else {
            if (stockDetail) {
                [detailDelegate getStockDetailFailed:GENERIC_ERROR_MESSAGE];
            }else {
                [delegate getStockListFailed:GENERIC_ERROR_MESSAGE];
            }
        }
        
    }
}

#pragma mark - NSXMLParserDelegate Methods

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"StocknIndexesResponseList"]) {
        stockArray = [[NSMutableArray alloc] init];
    }else if ([elementName isEqualToString:@"StockandIndex"]) {
        stockObject = [[Stock alloc] init];
    }else if ([elementName isEqualToString:@"StocknIndexesGraphicInfos"]) {
        stockDetailArray = [[NSMutableArray alloc] init];
    }else if ([elementName isEqualToString:@"StockandIndexGraphic"]) {
        stockDetailObject = [[StockPrice alloc] init];
    }else if ([elementName isEqualToString:@"IMKB100List"]) {
        imkb100 = [[NSMutableArray alloc] init];
    }else if ([elementName isEqualToString:@"IMKB50List"]) {
        imkb50 = [[NSMutableArray alloc] init];
    }else if ([elementName isEqualToString:@"IMKB30List"]) {
        imkb30 = [[NSMutableArray alloc] init];
    }else if ([elementName isEqualToString:@"IMKB100"] || [elementName isEqualToString:@"IMKB50"] || [elementName isEqualToString:@"IMKB30"]) {
        imkbObj = [[imkbListObject alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if (parser == xmlParser){
        if (!XMLTag){
            self.XMLTag = [[NSMutableString alloc]init];
        }
        XMLTag = (NSMutableString *) [XMLTag stringByAppendingString:string];
    }
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName

{
    if ([elementName isEqualToString:@"Success"]) {
        if (!XMLTag) {
            success = @"";
            return;
        }
        success = XMLTag;
    }else if ([elementName isEqualToString:@"Symbol"]) {
        if (!XMLTag) {
            stockObject.symbol = @"";
            return;
        }
        if(stockObject) {
            stockObject.symbol = XMLTag;
        }
        if (imkbObj) {
            imkbObj.symbol = XMLTag;
        }
    }else if ([elementName isEqualToString:@"Price"]) {
        if (!XMLTag) {
            stockObject.price = 0;
            return;
        }
        if (stockObject) {
            stockObject.price = [XMLTag floatValue];
        }else if (stockDetailObject) {
            stockDetailObject.price = [XMLTag floatValue];
        }
    }else if ([elementName isEqualToString:@"Difference"]) {
        if (!XMLTag) {
            stockObject.difference = 0;
            return;
        }
        stockObject.difference = [XMLTag floatValue];
    }else if ([elementName isEqualToString:@"Volume"]) {
        if (!XMLTag) {
            stockObject.volume = 0;
            return;
        }
        stockObject.volume = [XMLTag intValue];
    }else if ([elementName isEqualToString:@"Buying"]) {
        if (!XMLTag) {
            stockObject.buying = 0;
            return;
        }
        stockObject.buying = [XMLTag floatValue];
    }else if ([elementName isEqualToString:@"Selling"]) {
        if (!XMLTag) {
            stockObject.selling = 0;
            return;
        }
        stockObject.selling = [XMLTag floatValue];
    }else if ([elementName isEqualToString:@"Hour"]) {
        if (!XMLTag) {
            stockObject.hourInSeconds = 0;
            return;
        }
        stockObject.hourInSeconds = [XMLTag doubleValue];
    }else if ([elementName isEqualToString:@"StockandIndex"]) {
        [stockArray addObject:stockObject];
    }else if ([elementName isEqualToString:@"Date"]) {
        if (!XMLTag) {
            stockDetailObject.dateString = @"";
            return;
        }
        stockDetailObject.dateString = XMLTag;
    }else if ([elementName isEqualToString:@"StockandIndexGraphic"]) {
        [stockDetailArray addObject:stockDetailObject];
    }else if ([elementName isEqualToString:@"Name"]) {
        if (!XMLTag) {
            imkbObj.name = @"";
            return;
        }
        imkbObj.name = XMLTag;
    }else if ([elementName isEqualToString:@"Gain"]) {
        if (!XMLTag) {
            imkbObj.gain = 0;
            return;
        }
        imkbObj.gain = [XMLTag floatValue];
    }else if ([elementName isEqualToString:@"Fund"]) {
        if (!XMLTag) {
            imkbObj.fund = 0;
            return;
        }
        imkbObj.fund = [XMLTag floatValue];
    }else if ([elementName isEqualToString:@"IMKB100"]) {
        [imkb100 addObject:imkbObj];
    }else if ([elementName isEqualToString:@"IMKB50"]) {
        [imkb50 addObject:imkbObj];
    }else if ([elementName isEqualToString:@"IMKB30"]) {
        [imkb30 addObject:imkbObj];
    }
    XMLTag = nil;
}
@end
