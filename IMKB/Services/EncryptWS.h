//
//  EncryptWS.h
//  IMKB
//
//  Created by Macbook Air on 27/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "DateFormat.h"

@protocol EncryptServiceDelegate <NSObject>
-(void) onEncryptSucceeded:(NSString *) success;
-(void) onEncryptFailed:(NSString *) error;
@end

@interface EncryptWS : NSObject <NSXMLParserDelegate>{
    
    NSMutableURLRequest                         *ServiceUrl;
    NSURLConnection                             *ServiceConnection;
    NSMutableData                               *ServiceReceivedData;
    NSXMLParser                                 *xmlParser;
    NSMutableString                             *XMLTag;
    
    NSString                                    *resultSet;
    
}

@property(nonatomic,retain)id<EncryptServiceDelegate> delegate;
@property(nonatomic,retain)NSMutableString *XMLTag;

- (id)initWithDelegate:(id<EncryptServiceDelegate>) del;

-(void) callEncryptService;

@end
