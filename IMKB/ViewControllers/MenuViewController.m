//
//  MenuViewController.m
//  IMKB
//
//  Created by Macbook Air on 27/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import "MenuViewController.h"

@implementation MenuViewController

#pragma mark - View Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callStockListService];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - IBActions

- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)stocknIndexesAction:(id)sender {
    [self openListViewController:0 withButton:(UIButton*)sender];
}

- (IBAction)IMKBUptrendAction:(id)sender {
    [self openListViewController:1 withButton:(UIButton*)sender];
}

- (IBAction)IMKBDowntrendAction:(id)sender {
    [self openListViewController:2 withButton:(UIButton*)sender];
}

- (IBAction)IMKBVolume30Action:(id)sender {
    [self openVolumeListViewController:0 withButton:(UIButton*)sender];
}

- (IBAction)IMKBVolume50Action:(id)sender {
    [self openVolumeListViewController:1 withButton:(UIButton*)sender];
}

- (IBAction)IMKBVolume100Action:(id)sender {
    [self openVolumeListViewController:2 withButton:(UIButton*)sender];
}

-(void) openListViewController : (int) order withButton:(UIButton*) sender{
    ListViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ListVC"];
    switch (order) {
        case 0:
            [vc setStockList:stocknIndexes withTitle:sender.currentTitle];
            break;
        case 1:
            [vc setStockList:IMKBUptrend withTitle:sender.currentTitle];
            break;
        case 2:
            [vc setStockList:IMKBDowntrend withTitle:sender.currentTitle];
            break;
               default:
            break;
    }
    [self.navigationController pushViewController:vc animated:YES];
}

-(void) openVolumeListViewController : (int) order withButton:(UIButton*) sender{
    VolumeListViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VolumeListVC"];
    switch (order) {
        case 0:
            [vc setStockList:IMKBVolume30 withTitle:sender.currentTitle];
            break;
        case 1:
            [vc setStockList:IMKBVolume50 withTitle:sender.currentTitle];
            break;
        case 2:
            [vc setStockList:IMKBVolume100 withTitle:sender.currentTitle];
            break;
        default:
            break;
    }
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - StockList Service & Delegate

-(void) callStockListService {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    StockListWS *ws = [[StockListWS alloc] initWithDelegate:self];
    [ws getStockList:@"" withPeriod:DAY];
}

-(void)getStockListSucceeded:(NSMutableArray *)stockArray imkb100List:(NSMutableArray *)imkb100 imkb50List:(NSMutableArray *)imkb50 imkb30List:(NSMutableArray *)imkb30 {
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    stocknIndexes = [[NSMutableArray alloc] init];
    IMKBUptrend = [[NSMutableArray alloc] init];
    IMKBDowntrend = [[NSMutableArray alloc] init];
    IMKBVolume30 = [[NSMutableArray alloc] init];
    IMKBVolume50 = [[NSMutableArray alloc] init];
    IMKBVolume100 = [[NSMutableArray alloc] init];
    
    stocknIndexes = stockArray;
    [self splitArray];
    IMKBVolume30 = imkb30;
    IMKBVolume50 = imkb50;
    IMKBVolume100 = imkb100;
}

-(void)getStockListFailed:(NSString *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [SimpleAlert showSimpleAlert:error withController:self];
}

-(void) splitArray {
    for (int i=0; i<stocknIndexes.count; i++) {
        if ([[stocknIndexes objectAtIndex:i] difference] > 0) {
            [IMKBUptrend addObject:[stocknIndexes objectAtIndex:i]];
        }else {
            [IMKBDowntrend addObject:[stocknIndexes objectAtIndex:i]];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
