//
//  ListViewController.m
//  IMKB
//
//  Created by Macbook Air on 28/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import "ListViewController.h"

@implementation ListViewController

#pragma mark - View Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [_listTitleLabel setText:listTitle];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setStockList:(NSMutableArray *)stockList withTitle:(NSString *)title {
    listTitle = title;
    stockArray = [[NSMutableArray alloc] init];
    stockArray = stockList;
    filteredArray = stockList;
}

#pragma mark - IBActions

- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource & Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [filteredArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"StockCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    UIImageView *imgDiff = (UIImageView *) [cell viewWithTag:101];
    if([[filteredArray objectAtIndex:indexPath.row] difference] > 0) {
        [imgDiff setImage:[UIImage imageNamed:@"up-green"]];
    }else {
        [imgDiff setImage:[UIImage imageNamed:@"down-red"]];
    }
    
    UILabel *symbolLabel = (UILabel *) [cell viewWithTag:102];
    [symbolLabel setText:[[filteredArray objectAtIndex:indexPath.row] symbol]];
    
    UILabel *priceLabel = (UILabel *) [cell viewWithTag:103];
    [priceLabel setText:[NSString stringWithFormat:@"%.3f TL",[[filteredArray objectAtIndex:indexPath.row] price]]];
    
    UILabel *diffLabel = (UILabel *) [cell viewWithTag:104];
    [diffLabel setText:[NSString stringWithFormat:@"%%%.3f",[[filteredArray objectAtIndex:indexPath.row] difference]]];
    
    UILabel *volumeLabel = (UILabel *) [cell viewWithTag:105];
    [volumeLabel setText:[NSString stringWithFormat:@"%d",[[filteredArray objectAtIndex:indexPath.row] volume]]];
    
    UILabel *buyingLabel = (UILabel *) [cell viewWithTag:106];
    [buyingLabel setText:[NSString stringWithFormat:@"%.3f",[[filteredArray objectAtIndex:indexPath.row] buying]]];
    
    UILabel *sellingLabel = (UILabel *) [cell viewWithTag:107];
    [sellingLabel setText:[NSString stringWithFormat:@"%.3f",[[filteredArray objectAtIndex:indexPath.row] selling]]];
    
    UILabel *hourLabel = (UILabel *) [cell viewWithTag:108];
    [hourLabel setText:[DateFormat getTimeStringFromSeconds:[[filteredArray objectAtIndex:indexPath.row] hourInSeconds]]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return CELLHEIGHT;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
    [vc setStock:[filteredArray objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UISearchBarDelegate

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length == 0) {
        filteredArray = [stockArray copy];
    }else {
        filteredArray = [[NSMutableArray alloc] init];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"symbol CONTAINS[cd] %@ OR symbol LIKE[cd] %@",searchText, searchText];
        filteredArray = [[NSMutableArray arrayWithArray:[stockArray filteredArrayUsingPredicate:predicate]] mutableCopy];
    }
    [_stockTableView reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void) dismissKeyboard
{
    [_stockSearchBar resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
