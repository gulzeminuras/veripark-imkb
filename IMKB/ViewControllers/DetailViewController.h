//
//  DetailViewController.h
//  IMKB
//
//  Created by Macbook Air on 28/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BEMSimpleLineGraphView.h"
#import "StockListWS.h"
#import "MBProgressHUD.h"
#import "DateFormat.h"
#import "SimpleAlert.h"

@interface DetailViewController : UIViewController <BEMSimpleLineGraphDataSource, BEMSimpleLineGraphDelegate, StockDetailServiceDelegate> {
    Stock *stock;
    NSMutableArray *stockGraphArray;
}

@property (strong, nonatomic) IBOutlet BEMSimpleLineGraphView *graphView;

@property (strong, nonatomic) IBOutlet UILabel *symbolLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *differenceLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imgDifference;
@property (strong, nonatomic) IBOutlet UILabel *peakPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *lowestPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *finalLabel;
@property (strong, nonatomic) IBOutlet UILabel *volumeLabel;
@property (strong, nonatomic) IBOutlet UILabel *quantityLabel;
@property (strong, nonatomic) IBOutlet UILabel *graphDetailLabel;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

-(void) setStock :(Stock *) requestedStock;


@end
