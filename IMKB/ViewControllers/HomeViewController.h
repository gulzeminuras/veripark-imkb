//
//  HomeViewController.h
//  IMKB
//
//  Created by Macbook Air on 27/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EncryptWS.h"
#import "SimpleAlert.h"
#import "MBProgressHUD.h"

@interface HomeViewController : UIViewController <EncryptServiceDelegate>

@end
