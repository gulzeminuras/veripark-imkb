//
//  VolumeListViewController.h
//  IMKB
//
//  Created by Macbook Air on 29/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "imkbListObject.h"
#import "Constants.h"

@interface VolumeListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
    
    NSMutableArray *volumeArray;
    NSMutableArray *filteredArray;
    NSString *listTitle;
}

@property (strong, nonatomic) IBOutlet UILabel *listTitleLabel;
@property (strong, nonatomic) IBOutlet UITableView *volumeTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

-(void) setStockList : (NSMutableArray *) stockList withTitle: (NSString *) title;

@end
