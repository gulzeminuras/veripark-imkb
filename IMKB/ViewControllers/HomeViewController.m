//
//  HomeViewController.m
//  IMKB
//
//  Created by Macbook Air on 27/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import "HomeViewController.h"

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    EncryptWS *ws = [[EncryptWS alloc]initWithDelegate:self];
    [ws callEncryptService];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Encryption Service Delegate

-(void)onEncryptFailed:(NSString *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [SimpleAlert showSimpleAlert:error withController:self];
}

-(void)onEncryptSucceeded:(NSString *)success {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSLog(@"%@", success);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
