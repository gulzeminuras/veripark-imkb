//
//  VolumeListViewController.m
//  IMKB
//
//  Created by Macbook Air on 29/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import "VolumeListViewController.h"

@interface VolumeListViewController ()

@end

@implementation VolumeListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_listTitleLabel setText:listTitle];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setStockList:(NSMutableArray *)stockList withTitle:(NSString *)title {
    listTitle = title;
    volumeArray = [[NSMutableArray alloc] init];
    volumeArray = stockList;
    filteredArray = stockList;
}

#pragma mark - IBActions

- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource & Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [filteredArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"VolumeListCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    UILabel *symbolLabel = (UILabel *) [cell viewWithTag:102];
    [symbolLabel setText:[[filteredArray objectAtIndex:indexPath.row] symbol]];
    
    UILabel *nameLabel = (UILabel *) [cell viewWithTag:103];
    [nameLabel setText:[[filteredArray objectAtIndex:indexPath.row] name]];
    
    UILabel *gainLabel = (UILabel *) [cell viewWithTag:104];
    [gainLabel setText:[NSString stringWithFormat:@"%.3f",[[filteredArray objectAtIndex:indexPath.row] gain]]];
    
    UILabel *fundLabel = (UILabel *) [cell viewWithTag:105];
    [fundLabel setText:[NSString stringWithFormat:@"%.3f",[[filteredArray objectAtIndex:indexPath.row] fund]]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return CELLHEIGHT;
}


#pragma mark - UISearchBarDelegate

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length == 0) {
        filteredArray = [volumeArray copy];
    }else {
        filteredArray = [[NSMutableArray alloc] init];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"symbol CONTAINS[cd] %@ OR symbol LIKE[cd] %@ OR name LIKE[cd] %@ OR name LIKE[cd] %@",searchText, searchText, searchText, searchText];
        filteredArray = [[NSMutableArray arrayWithArray:[volumeArray filteredArrayUsingPredicate:predicate]] mutableCopy];
    }
    [_volumeTableView reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void) dismissKeyboard
{
    [_searchBar resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
