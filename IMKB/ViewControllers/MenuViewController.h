//
//  MenuViewController.h
//  IMKB
//
//  Created by Macbook Air on 27/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListViewController.h"
#import "VolumeListViewController.h"

@interface MenuViewController : UIViewController <StockListServiceDelegate> {
    
    NSMutableArray *stocknIndexes;
    NSMutableArray *IMKBUptrend;
    NSMutableArray *IMKBDowntrend;
    NSMutableArray *IMKBVolume30;
    NSMutableArray *IMKBVolume50;
    NSMutableArray *IMKBVolume100;

}

@end
