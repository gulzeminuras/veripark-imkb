//
//  DetailViewController.m
//  IMKB
//
//  Created by Macbook Air on 28/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import "DetailViewController.h"

@implementation DetailViewController

#pragma mark - View Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setStockDetails];
    [self setGraphOptions];
    [self callStockDetailService : MONTH];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnDayAction:(id)sender {
    [self callStockDetailService : DAY];
}

- (IBAction)btnWeekAction:(id)sender {
    [self callStockDetailService : WEEK];
}
- (IBAction)btnMonthAction:(id)sender {
    [self callStockDetailService : MONTH];
}

#pragma mark - Initial Settings
-(void) setStock :(Stock *) requestedStock {
    stock = [[Stock alloc] init];
    stock = requestedStock;
}

-(void) setGraphOptions {
    _graphView.enableTouchReport = YES;
    _graphView.enablePopUpReport = YES;
    _graphView.enableYAxisLabel = YES;
    _graphView.autoScaleYAxis = YES;
    _graphView.alwaysDisplayDots = NO;
    _graphView.enableReferenceXAxisLines = YES;
    _graphView.enableReferenceYAxisLines = YES;
    _graphView.enableReferenceAxisFrame = YES;
    
    _graphView.averageLine.enableAverageLine = YES;
    _graphView.averageLine.alpha = 0.6;
    _graphView.averageLine.color = [UIColor lightGrayColor];
    _graphView.averageLine.width = 2.5;
    _graphView.averageLine.dashPattern = @[@(2),@(2)];
    
    _graphView.animationGraphStyle = BEMLineAnimationDraw;
    
    _graphView.lineDashPatternForReferenceYAxisLines = @[@(2),@(2)];
    
    _graphView.formatStringForValues = @"%.3f";

}

-(void) setStockDetails {
    [_titleLabel setText:[stock symbol]];
    [_symbolLabel setText:[stock symbol]];
    [_priceLabel setText:[NSString stringWithFormat:@"%.3f", [stock price]]];
    [_differenceLabel setText:[NSString stringWithFormat:@"%.3f", [stock difference]]];
    if([stock difference] > 0) {
        [_imgDifference setImage:[UIImage imageNamed:@"up-green"]];
    }else {
        [_imgDifference setImage:[UIImage imageNamed:@"down-red"]];
    }
    [_peakPriceLabel setText:[NSString stringWithFormat:@"%.3f", [stock dayPeakPrice]]];
    [_lowestPriceLabel setText:[NSString stringWithFormat:@"%.3f", [stock dayLowestPrice]]];
    [_volumeLabel setText:[NSString stringWithFormat:@"%d", [stock volume]]];
    [_quantityLabel setText:[NSString stringWithFormat:@"%d", [stock total]]];

}

#pragma mark - SimpleLineGraphDelegate & Data Source

- (NSInteger)numberOfPointsInLineGraph:(BEMSimpleLineGraphView *)graph {
    return [stockGraphArray count];
}

- (CGFloat)lineGraph:(BEMSimpleLineGraphView *)graph valueForPointAtIndex:(NSInteger)index {
    return [[stockGraphArray objectAtIndex:index] price];
}

- (NSString *)lineGraph:(BEMSimpleLineGraphView *)graph labelOnXAxisForIndex:(NSInteger)index {
    return [DateFormat dateStringToSimpleString:[[stockGraphArray objectAtIndex:index] dateString] withYear:NO];
}

- (void)lineGraph:(BEMSimpleLineGraphView *)graph didTouchGraphWithClosestIndex:(NSInteger)index {
    [_graphDetailLabel setText:[NSString stringWithFormat:@"Tarih : %@  -  Fiyat : %.4f", [DateFormat dateStringToSimpleString:[[stockGraphArray objectAtIndex:index] dateString] withYear:YES], [[stockGraphArray objectAtIndex:index] price]]];
}

- (void)lineGraph:(BEMSimpleLineGraphView *)graph didReleaseTouchFromGraphWithClosestIndex:(CGFloat)index {
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _graphDetailLabel.alpha = 0.0;
        _graphDetailLabel.alpha = 0.0;
    } completion:^(BOOL finished) {
        [_graphDetailLabel setText:@""];
        _graphDetailLabel.alpha = 1.0;
    }];
}


#pragma mark - StockDetail Service & Delegate

-(void) callStockDetailService:(NSString *) period{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    StockListWS *ws = [[StockListWS alloc] initWithDetailDelegate:self];
    [ws getStockList:[NSString stringWithFormat:@"<tem:RequestedSymbol>%@</tem:RequestedSymbol>" ,[stock symbol]] withPeriod:period];
//   [ws getStockList:@"MIPAZ" withPeriod:period];
}

-(void)getStockDetailSucceeded:(NSMutableArray *)stockDetailArray {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    stockGraphArray = [[NSMutableArray alloc] init];
    stockGraphArray = [[[stockDetailArray reverseObjectEnumerator] allObjects] mutableCopy];
    [_finalLabel setText:[NSString stringWithFormat:@"%.3f", [[stockDetailArray objectAtIndex:0] price]]];
    [_graphView reloadGraph];
}

-(void)getStockDetailFailed:(NSString *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [SimpleAlert showSimpleAlert:error withController:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
