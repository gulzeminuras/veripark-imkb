//
//  ListViewController.h
//  IMKB
//
//  Created by Macbook Air on 28/08/16.
//  Copyright © 2016 Macbook Air. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"

@interface ListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
    
    NSMutableArray *stockArray;
    NSMutableArray *filteredArray;
    NSString *listTitle;
}

@property (strong, nonatomic) IBOutlet UILabel *listTitleLabel;
@property (strong, nonatomic) IBOutlet UITableView *stockTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *stockSearchBar;

-(void) setStockList : (NSMutableArray *) stockList withTitle: (NSString *) title;

@end
